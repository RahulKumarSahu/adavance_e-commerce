<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUserRequest;
use App\Models\Admin;
use Carbon\Carbon;
use Image;
use Hash;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    public function allAdminRole()
    {
        $adminUser = Admin::where('type', 2)->latest()->get();
        return view('backend.role.all_admin_role', compact('adminUser'));
    }
    public function addAdminRole()
    {
        $adminUser = Admin::where('type', 2)->latest()->get();
        return view('backend.role.add_admin_role', compact('adminUser'));
    }
    public function storeAdminRole(AdminUserRequest $request)

    {
        $image = $request->file('profile_photo_path');
        $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize(225, 255)->save('upload/admin_images/' . $name_gen);
        $save_url = 'upload/admin_images/' . $name_gen;

        Admin::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'brand' => $request->brand,
            'category' => $request->category,
            'product' => $request->product,
            'slider' => $request->slider,
            'coupons' => $request->coupons,

            'shipping' => $request->shipping,
            'orders' => $request->orders,
            'reports' => $request->reports,
            'alluser' => $request->alluser,
            'setting' => $request->setting,
            'order_return' => $request->order_return,
            'admin_user_role' => $request->admin_user_role,
            'type' => 2,
            'profile_photo_path' => $save_url,
            'created_at' => Carbon::now(),
        ]);
        $notification = array(
            'message' => 'Admin Inserted Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('all.admin.user')->with($notification);
    }

    public function editAdminRole($id)
    {
        $adminUser = Admin::findOrFail($id);
        return view('backend.role.edit_admin_role', compact('adminUser'));
    }

    public function updateAdminRole(Request $request)
    {
        $admin_id = $request->id;
        $old_image = $request->old_image;
        if ($request->file('profile_photo_path')) {

            @unlink($old_image);
            $image = $request->file('profile_photo_path');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(255, 255)->save('upload/admin_images/' . $name_gen);
            $save_url = 'upload/admin_images/' . $name_gen;

            Admin::findOrFail($admin_id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'brand' => $request->brand,
                'category' => $request->category,
                'product' => $request->product,
                'slider' => $request->slider,
                'coupons' => $request->coupons,

                'shipping' => $request->shipping,
                'orders' => $request->orders,
                'reports' => $request->reports,
                'alluser' => $request->alluser,
                'setting' => $request->setting,
                'order_return' => $request->order_return,
                'admin_user_role' => $request->admin_user_role,
                'type' => 2,
                'profile_photo_path' => $save_url,
                'created_at' => Carbon::now(),
            ]);
            $notification = array(
                'message' => 'Admin User Updated with Image Successfully',
                'alert-type' => 'info'
            );
            return redirect()->route('all.admin.user')->with($notification);
        } else {
            Admin::findOrFail($admin_id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'brand' => $request->brand,
                'category' => $request->category,
                'product' => $request->product,
                'slider' => $request->slider,
                'coupons' => $request->coupons,

                'shipping' => $request->shipping,
                'orders' => $request->orders,
                'reports' => $request->reports,
                'alluser' => $request->alluser,
                'setting' => $request->setting,
                'order_return' => $request->order_return,
                'admin_user_role' => $request->admin_user_role,
                'type' => 2,
                'created_at' => Carbon::now(),
            ]);
            $notification = array(
                'message' => 'Admin User Updated without Image Successfully',
                'alert-type' => 'info'
            );
            return redirect()->route('all.admin.user')->with($notification);
        }
    }
    public function deleteAdminRole($id)
    {
        $adminUser = Admin::findOrFail($id);
        $img = $adminUser->profile_photo_path;
        @unlink($img);
        $adminUser->delete();

        $notification = array(
            'message' => 'Admin User Deleted Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
}