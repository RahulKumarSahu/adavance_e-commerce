<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use PDF;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function pendingOrders()
    {
        $orders = Order::where('status', 'pending')->orderBy('id', 'DESC')->get();
        return view('backend.order.pending_order', compact('orders'));
    }

    public function pendingOrderDetail($order_id)
    {
        $orders = Order::with('division', 'district', 'state', 'user')->where('id', $order_id)->where('user_id', Auth::id())->first();
        $orderItem = OrderItem::with('product')->where('order_id', $order_id)->orderBy('id', 'DESC')->get();
        return view('backend.order.pending_order_detail', compact('orders', 'orderItem'));
    }

    //Confirmed Orders

    public function confirmedOrders()
    {
        $orders = Order::where('status', 'confirmed')->orderBy('id', 'DESC')->get();
        return view('backend.order.confirmed_order', compact('orders'));
    }
    //processing Orders

    public function processingOrders()
    {
        $orders = Order::where('status', 'processing')->orderBy('id', 'DESC')->get();
        return view('backend.order.processing_order', compact('orders'));
    }
    //picked Orders

    public function pickedOrders()
    {
        $orders = Order::where('status', 'picked')->orderBy('id', 'DESC')->get();
        return view('backend.order.picked_order', compact('orders'));
    }
    //shipped Orders

    public function shippedOrders()
    {
        $orders = Order::where('status', 'shipped')->orderBy('id', 'DESC')->get();
        return view('backend.order.shipped_order', compact('orders'));
    }
    //delivered Orders

    public function deliveredOrders()
    {
        $orders = Order::where('status', 'delivered')->orderBy('id', 'DESC')->get();
        return view('backend.order.delivered_order', compact('orders'));
    }
    //cancel Orders

    public function cancelOrders()
    {
        $orders = Order::where('status', 'cancel')->orderBy('id', 'DESC')->get();
        return view('backend.order.cancel_order', compact('orders'));
    }

    //Pending To Confirm

    public function pendingToConfirm($order_id)
    {
        $orders = Order::findOrFail($order_id)->update(['status' => 'confirmed']);

        $notification = array(
            'message' => 'Order Confirm Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('confirmed-orders')->with($notification);
    }

    //Confirm To Processing

    public function confirmToProcessing($order_id)
    {
        $orders = Order::findOrFail($order_id)->update(['status' => 'processing']);

        $notification = array(
            'message' => 'Order Processing Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('processing-orders')->with($notification);
    }
    //Processing To Picked

    public function processingToPicked($order_id)
    {
        $orders = Order::findOrFail($order_id)->update(['status' => 'picked']);

        $notification = array(
            'message' => 'Order Picked Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('picked-orders')->with($notification);
    }
    //Picked To Shipped

    public function pickedToShipped($order_id)
    {
        $orders = Order::findOrFail($order_id)->update(['status' => 'shipped']);

        $notification = array(
            'message' => 'Order Shipped Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('shipped-orders')->with($notification);
    }

    //Shipped To Delivered

    public function shippedToDelivered($order_id)
    {
        $orders = Order::findOrFail($order_id)->update(['status' => 'delivered']);

        $notification = array(
            'message' => 'Order Delivered Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('delivered-orders')->with($notification);
    }

    //Delivered To Cancel

    public function deliveredToCancel($order_id)
    {
        $orders = Order::findOrFail($order_id)->update(['status' => 'cancel']);

        $notification = array(
            'message' => 'Order Cancel Successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('cancel-orders')->with($notification);
    }

    public function invoiceDownload($order_id)
    {
        $orders = Order::with('division', 'district', 'state', 'user')->where('id', $order_id)->first();
        $orderItem = OrderItem::with('product')->where('id', $order_id)->orderBy('id', 'DESC')->get();
        $pdf = PDF::loadView('backend.order.order_invoice', compact('orders', 'orderItem'))->setPaper('a4')->SetOptions([
            'tempdr' => public_path(),
            'chroot' => public_path(),
        ]);
        return $pdf->download('invoice.pdf');
    }
}