<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use DateTime;
use FontLib\Table\Type\maxp;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function allReports()
    {
        return view('backend.report.report_view');
    }

    public function searchByDate(Request $request)
    {
        // return $request->all();
        $date = new DateTime($request->date);
        $formateDate = $date->format('d F Y');
        $orders = Order::where('order_date', $formateDate)->latest()->get();
        return view('backend.report.report_show', compact('orders'));
    }

    public function searchByMonth(Request $request)
    {
        $orders = Order::where('order_month', $request->month)->where('order_year', $request->year_name)->latest()->get();
        return view('backend.report.report_show', compact('orders'));
    }

    public function searchByYear(Request $request)
    {
        $orders = Order::where('order_year', $request->year_name)->latest()->get();
        return view('backend.report.report_show', compact('orders'));
    }

    public function allUserReports()
    {
        $users =  User::latest()->get();
        return view('backend.user.all_user', compact('users'));
    }
}