<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SliderRequest;
use App\Models\Slider;

use\Image;

use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::latest()->get();

        return view('backend.slider.index', compact('sliders'));
    }

    public function store(SliderRequest $request)
    {
        $image = $request->file('slider_img');
        $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize(870, 370)->save('upload/slider/' . $name_gen);
        $save_url = 'upload/slider/' . $name_gen;

        Slider::insert([
            'title' => $request->title,
            'description' => $request->description,
            'slider_img' => $save_url,
        ]);
        $notification = array(
            'message' => 'Slider Inserted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }
    public function sliderEdit($id)
    {
        $sliders = Slider::findOrFail($id);
        return view('backend.slider.edit', compact('sliders'));
    }
    public function sliderUpdate(Request $request)
    {
        $slider_id = $request->id;
        $old_image = $request->old_image;
        if ($request->file('slider_img')) {

            @unlink($old_image);
            $image = $request->file('slider_img');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(870, 370)->save('upload/slider/' . $name_gen);
            $save_url = 'upload/slider/' . $name_gen;

            Slider::findOrFail($slider_id)->update([
                'title' => $request->title,
                'description' => $request->description,
                'slider_img' => $save_url,
            ]);
            $notification = array(
                'message' => 'Slider Updated With Image Successfully',
                'alert-type' => 'info'
            );
            return redirect()->route('slider')->with($notification);
        } else {
            Slider::findOrFail($slider_id)->update([
                'title' => $request->title,
                'description' => $request->description,

            ]);
            $notification = array(
                'message' => 'Slider Updated Without Image Successfully',
                'alert-type' => 'info'
            );
            return redirect()->route('slider')->with($notification);
        }
    }
    public function sliderDelete($id)
    {
        $slider = Slider::findOrFail($id);
        $img = $slider->slider_img;
        unlink($img);
        $slider->delete();

        $notification = array(
            'message' => 'Brand Deleted Successfully',
            'alert-type' => 'info'
        );
        return redirect()->back()->with($notification);
    }
    public function sliderInactive($id)
    {
        Slider::findOrFail($id)->update(['status' => 0]);

        $notification = array(
            'message' => 'Slider Inactive',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
    public function slideractive($id)
    {
        Slider::findOrFail($id)->update(['status' => 1]);


        $notification = array(
            'message' => 'Slider Active',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
