<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchProduct(Request $request)
    {
        $request->validate(["search" => "required"]);
        $item = $request->search;
        // echo $item;

        $products = Product::where('product_name_en', 'LIKE', "%$item%")->get();
        $categories = Category::orderBy('category_name_en', 'ASC')->get();
        return view('frontend.product.product_search', compact('products', 'categories'));
    }
    public function adSearchProduct(Request $request)
    {
        // return $request;
        $request->validate(["search" => "required"]);
        $item = $request->search;
        // echo $item;
        $products = Product::where('product_name_en', 'LIKE', "%$item%")
            ->select('product_name_en', 'product_thambnail', 'selling_price', 'id', 'product_slug_en')
            ->limit(5)->get();
        return view('frontend.product.advance_search', compact('products'));
    }
}