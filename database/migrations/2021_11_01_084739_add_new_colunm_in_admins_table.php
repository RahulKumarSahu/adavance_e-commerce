<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColunmInAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {

            $table->string('phone')->nullable()->after('email');
            $table->string('brand')->nullable()->after('phone');
            $table->string('category')->nullable()->after('brand');
            $table->string('product')->nullable()->after('category');
            $table->string('slider')->nullable()->after('product');
            $table->string('coupons')->nullable()->after('slider');
            $table->string('shipping')->nullable()->after('coupons');

            $table->string('orders')->nullable()->after('shipping');
            $table->string('reports')->nullable()->after('orders');
            $table->string('alluser')->nullable()->after('reports');
            $table->string('setting')->nullable()->after('alluser');
            $table->string('order_return')->nullable()->after('setting');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('brand');
            $table->dropColumn('category');
            $table->dropColumn('product');
            $table->dropColumn('slider');
            $table->dropColumn('coupons');
            $table->dropColumn('shipping');
            $table->dropColumn('orders');
            $table->dropColumn('reports');
            $table->dropColumn('alluser');
            $table->dropColumn('setting');
            $table->dropColumn('order_return');
        });
    }
}
