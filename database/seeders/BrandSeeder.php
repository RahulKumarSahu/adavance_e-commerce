<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = ([
            [
                'brand_name_en' => 'Apple',
                'brand_name_hin' => 'एप्पल',
                'brand_slug_en' => 'apple',
                'brand_slug_hin' => 'एप्पल',
                'brand_image' => 'null',
            ],
            [
                'brand_name_en' => 'Vivo',
                'brand_name_hin' => 'विवो',
                'brand_slug_en' => 'vivo',
                'brand_slug_hin' => 'विवो',
                'brand_image' => 'null',

            ],
            [
                'brand_name_en' => 'Xiaomi ',
                'brand_name_hin' => 'श्याओमी',
                'brand_slug_en' => 'xiaomi',
                'brand_slug_hin' => 'श्याओमी',
                'brand_image' => 'null',

            ],
            [
                'brand_name_en' => 'OnePlus',
                'brand_name_hin' => 'वनप्लस',
                'brand_slug_en' => 'oneplus',
                'brand_slug_hin' => 'वनप्लस',
                'brand_image' => 'null',

            ],
            [
                'brand_name_en' => 'Samsung ',
                'brand_name_hin' => 'सैमसंग',
                'brand_slug_en' => 'samsung',
                'brand_slug_hin' => 'सैमसंग',
                'brand_image' => 'null',

            ],

        ]);
        collect($brands)->each(function ($brand) {
            Brand::create($brand);
        });
    }
}