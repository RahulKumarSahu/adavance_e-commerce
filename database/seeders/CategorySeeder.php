<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ([
            [
                'category_name_en' => 'Electronics',
                'category_name_hin' => 'इलेक्ट्रॉनिक उपकरण',
                'category_slug_en' => 'electronics',
                'category_slug_hin' => 'इलेक्ट्रॉनिक-उपकरण',
                'category_icon' => 'fa fa-diamond',
            ],
            [
                'category_name_en' => 'Fashion',
                'category_name_hin' => 'सुंदरता',
                'category_slug_en' => 'fashion',
                'category_slug_hin' => 'सुंदरता',
                'category_icon' => 'fa fa-shopping-bag',
            ],
            [
                'category_name_en' => 'Health and Beauty',
                'category_name_hin' => 'आरोग्य और सुंदरता',
                'category_slug_en' => 'health-and-beauty',
                'category_slug_hin' => 'आरोग्य-और-सुंदरता',
                'category_icon' => 'fa fa-paper-plane',
            ],
            [
                'category_name_en' => 'Sports',
                'category_name_hin' => 'खेल',
                'category_slug_en' => 'sports',
                'category_slug_hin' => 'खेल',
                'category_icon' => 'fa fa-futbol-o',
            ],
            [
                'category_name_en' => 'Books',
                'category_name_hin' => 'पुस्तकें',
                'category_slug_en' => 'books',
                'category_slug_hin' => 'पुस्तकें',
                'category_icon' => 'fa fa-book',
            ],
            [
                'category_name_en' => 'Home Appliances',
                'category_name_hin' => 'घरेलू उपकरण',
                'category_slug_en' => 'home-appliances',
                'category_slug_hin' => 'घरेलू-उपकरण',
                'category_icon' => 'fa fa-home',
            ],
        ]);
        collect($categories)->each(function ($category) {
            Category::create($category);
        });
    }
}
