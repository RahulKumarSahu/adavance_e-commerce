<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ([
            [
                'brand_id' => '1',
                'category_id' => '1',
                'subcategory_id' => '1',
                'subsubcategory_id' => '1',
                'product_name_en' => 'APPLE iPhone 12 (White, 64 GB)',
                'product_name_hin' => 'ऐप्पल आईफोन 12 (सफेद, 64 जीबी)',
                'product_slug_en' => 'apple-iPhone-12-(white,-64-gb)',
                'product_slug_hin' => 'ऐप्पल-आईफोन-12-(सफेद,-64-जीबी)',
                'product_code' => '1000',
                'product_qty' => '20',
                'product_tags_en' => 'new,hotdeal',
                'product_tags_hin' => 'नया,ताज़ा सौदा',

                'product_color_en' => 'red,black,blue,white',
                'product_color_hin' => 'लाल,काला,नीला,सफेद',
                'selling_price' => '10000',
                'discount_price' => '9998',
                'short_descp_en' => '12MP TrueDepth Front Camera with Night Mode, 4K Dolby Vision HDR Recording',
                'short_descp_hin' => 'नाइट मोड के साथ 12MP ट्रूडेप्थ फ्रंट कैमरा, 4K डॉल्बी विजन एचडीआर रिकॉर्डिंग',
                'long_descp_en' => 'The iPhone 12 costs more than its predecessors but has a crisp new HDR OLED screen. It offers nearly all the feaures of the iPhone 12 Pro, minus some camera capabilities, but should be a good enough package for most users. Apple has returned to a flat aluminium frame but the iPhone 12 promises to be more durable thanks to its Ceramic Shield material on the front and IP68 rating.',
                'long_descp_hin' => 'IPhone 12 की कीमत अपने पूर्ववर्तियों की तुलना में अधिक है, लेकिन इसमें एक नई HDR OLED स्क्रीन है। यह iPhone 12 Pro की लगभग सभी सुविधाओं की पेशकश करता है, कुछ कैमरा क्षमताओं को घटाता है, लेकिन अधिकांश उपयोगकर्ताओं के लिए एक अच्छा पर्याप्त पैकेज होना चाहिए। Apple एक सपाट एल्यूमीनियम फ्रेम में लौट आया है, लेकिन iPhone 12 सामने और IP68 रेटिंग पर सिरेमिक शील्ड सामग्री के लिए अधिक टिकाऊ होने का वादा करता है।',
                'product_thambnail' => 'null',
                'featured' => '1',

            ],
            [
                'brand_id' => '1',
                'category_id' => '1',
                'subcategory_id' => '1',
                'subsubcategory_id' => '1',
                'product_name_en' => 'APPLE iPhone 11 Pro Max (White, 64 GB)',
                'product_name_hin' => 'ऐप्पल आईफोन 11-प्रो-मैक्स (सफेद, 64 जीबी)',
                'product_slug_en' => 'apple-iPhone-11-pro-max(white,-64-gb)',
                'product_slug_hin' => 'ऐप्पल-आईफोन-11-प्रो-मैक्स(सफेद,-64-जीबी)',
                'product_code' => '1000',
                'product_qty' => '20',
                'product_tags_en' => 'new,hotdeal',
                'product_tags_hin' => 'नया,ताज़ा सौदा',
                'product_color_en' => 'red,black,blue,white',
                'product_color_hin' => 'लाल,काला,नीला,सफेद',
                'selling_price' => '12000',
                'discount_price' => '11500',
                'short_descp_en' => '12MP TrueDepth Front Camera with Night Mode, 4K Dolby Vision HDR Recording',
                'short_descp_hin' => 'नाइट मोड के साथ 12MP ट्रूडेप्थ फ्रंट कैमरा, 4K डॉल्बी विजन एचडीआर रिकॉर्डिंग',
                'long_descp_en' => 'The iPhone 12 costs more than its predecessors but has a crisp new HDR OLED screen. It offers nearly all the feaures of the iPhone 12 Pro, minus some camera capabilities, but should be a good enough package for most users. Apple has returned to a flat aluminium frame but the iPhone 12 promises to be more durable thanks to its Ceramic Shield material on the front and IP68 rating.',
                'long_descp_hin' => 'IPhone 12 की कीमत अपने पूर्ववर्तियों की तुलना में अधिक है, लेकिन इसमें एक नई HDR OLED स्क्रीन है। यह iPhone 12 Pro की लगभग सभी सुविधाओं की पेशकश करता है, कुछ कैमरा क्षमताओं को घटाता है, लेकिन अधिकांश उपयोगकर्ताओं के लिए एक अच्छा पर्याप्त पैकेज होना चाहिए। Apple एक सपाट एल्यूमीनियम फ्रेम में लौट आया है, लेकिन iPhone 12 सामने और IP68 रेटिंग पर सिरेमिक शील्ड सामग्री के लिए अधिक टिकाऊ होने का वादा करता है।',
                'product_thambnail' => 'null',
                'featured' => '1',
            ],
        ]);
        collect($products)->each(function ($product) {
            Product::create($product);
        });
    }
}