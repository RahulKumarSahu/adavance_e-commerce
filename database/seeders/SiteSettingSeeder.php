<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class SiteSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('site_settings')->insert(array(
            array(
                'phone_one' => '9399887991',
                'phone_two' => '1234567890',
                'email' => 'rsahu8817@gmail.com',
                'company_name' => 'Easy Shop',
                'company_address' => 'Mahasamund,Chhattisgarh,India',
                'facebook' => 'https://www.facebook.com',
                'twitter' => 'https://www.twitter.com',
                'linkedin' => 'https://www.linkedin.com',
                'youtube' => 'https://www.youtube.com',
            )
        ));
    }
}
