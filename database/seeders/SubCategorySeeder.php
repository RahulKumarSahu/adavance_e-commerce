<?php

namespace Database\Seeders;

use App\Models\SubCategory;
use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = ([
            [
                'category_id' => '1',
                'subcategory_name_en' => 'Smart Phone',
                'subcategory_name_hin' => 'स्मार्टफोन',
                'subcategory_slug_en' => 'smart-phone',
                'subcategory_slug_hin' => 'स्मार्टफोन',
            ],
            [
                'category_id' => '1',
                'subcategory_name_en' => 'Laptop Accessories',
                'subcategory_name_hin' => 'लैपटॉप सहायक उपकरण',
                'subcategory_slug_en' => 'laptop-accessories',
                'subcategory_slug_hin' => 'लैपटॉप-सहायक-उपकरण',
            ],
            [
                'category_id' => '1',
                'subcategory_name_en' => 'tablets',
                'subcategory_name_hin' => 'टैबलेट ',
                'subcategory_slug_en' => 'tablets',
                'subcategory_slug_hin' => 'टैबलेट ',
                // 3
            ],
            [
                'category_id' => '2',
                'subcategory_name_en' => 'Mens Top Wear',
                'subcategory_name_hin' => 'मेन्स के टॉपवियर ',
                'subcategory_slug_en' => 'mens-top-wear',
                'subcategory_slug_hin' => 'मेन्स-के-टॉपवियर ',
                // 4
            ],
            [
                'category_id' => '2',
                'subcategory_name_en' => 'Mens Bottom Wear',
                'subcategory_name_hin' => 'मेन्स बॉटम वियर ',
                'subcategory_slug_en' => 'mens-bottom-wear',
                'subcategory_slug_hin' => 'मेन्स बॉटम वियर ',
                // 5
            ],
            [
                'category_id' => '2',
                'subcategory_name_en' => 'Womens Ethnic Wear',
                'subcategory_name_hin' => 'महिला जातीय पहनावा ',
                'subcategory_slug_en' => 'womens-ethnic-wear',
                'subcategory_slug_hin' => 'महिला-जातीय-पहनावा ',
                // 6
            ],
            [
                'category_id' => '2',
                'subcategory_name_en' => 'Womens Ethnic Bottom',
                'subcategory_name_hin' => 'महिला-जातीय-तल',
                'subcategory_slug_en' => 'womens-ethnic-bottom',
                'subcategory_slug_hin' => 'महिला-जातीय-तल',
                // 7
            ],
            [
                'category_id' => '3',
                'subcategory_name_en' => 'Bath And Body',
                'subcategory_name_hin' => 'स्नान और शरीर',
                'subcategory_slug_en' => 'bath-and-body',
                'subcategory_slug_hin' => 'स्नान-और-शरीर',
                // 8
            ],
            [
                'category_id' => '3',
                'subcategory_name_en' => 'Bath Salts',
                'subcategory_name_hin' => 'स्नान लवण',
                'subcategory_slug_en' => 'bath-salts',
                'subcategory_slug_hin' => 'स्नान-लवण',
                // 9
            ],
            [
                'category_id' => '4',
                'subcategory_name_en' => 'Cricket',
                'subcategory_name_hin' => 'क्रिकेट',
                'subcategory_slug_en' => 'cricket',
                'subcategory_slug_hin' => 'क्रिकेट',
                // 10

            ],
            [
                'category_id' => '4',
                'subcategory_name_en' => 'Badminton',
                'subcategory_name_hin' => 'बैडमिंटन',
                'subcategory_slug_en' => 'badminton',
                'subcategory_slug_hin' => 'बैडमिंटन',
                //11
            ],
            [
                'category_id' => '4',
                'subcategory_name_en' => 'Football',
                'subcategory_name_hin' => 'फ़ुटबॉल',
                'subcategory_slug_en' => 'football',
                'subcategory_slug_hin' => 'फ़ुटबॉल',
                // 12
            ],
            [
                'category_id' => '5',
                'subcategory_name_en' => 'Entrance Exam',
                'subcategory_name_hin' => 'प्रवेश परीक्षा',
                'subcategory_slug_en' => 'entrance-exam',
                'subcategory_slug_hin' => 'प्रवेश-परीक्षा',
                //13
            ],
            [
                'category_id' => '5',
                'subcategory_name_en' => 'Academic',
                'subcategory_name_hin' => 'शैक्षिक',
                'subcategory_slug_en' => 'academic',
                'subcategory_slug_hin' => 'शैक्षिक',
                //14
            ],
            [
                'category_id' => '6',
                'subcategory_name_en' => 'Coffee Grinder',
                'subcategory_name_hin' => 'कॉफी बनाने की मशीन',
                'subcategory_slug_en' => 'coffee-grinder',
                'subcategory_slug_hin' => 'कॉफी-बनाने-की-मशीन',
                //15
            ],
            [
                'category_id' => '6',
                'subcategory_name_en' => 'Water Purifiers & Accessories',
                'subcategory_name_hin' => 'जल शोधक और सहायक उपकरण',
                'subcategory_slug_en' => 'water-purifiers-&-accessories',
                'subcategory_slug_hin' => 'जल-शोधक-और-सहायक-उपकरण',
                //16
            ],
        ]);
        collect($subcategories)->each(function ($subcategory) {
            SubCategory::create($subcategory);
        });
    }
}