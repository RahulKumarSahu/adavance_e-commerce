<?php

namespace Database\Seeders;

use App\Models\SubSubCategory;
use Illuminate\Database\Seeder;

class SubSubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subsubcategories = ([
            [
                'category_id' => '1',
                'sub_category_id' => '1',
                'subsubcategory_name_en' => 'iphone',
                'subsubcategory_name_hin' => 'आई - फ़ोन',
                'subsubcategory_slug_en' => 'iphone',
                'subsubcategory_slug_hin' => 'आई-फ़ोन',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '1',
                'subsubcategory_name_en' => 'Samsung',
                'subsubcategory_name_hin' => 'सैमसंग',
                'subsubcategory_slug_en' => 'samsung',
                'subsubcategory_slug_hin' => 'सैमसंग',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '1',
                'subsubcategory_name_en' => 'Redmi',
                'subsubcategory_name_hin' => 'रेड्मी',
                'subsubcategory_slug_en' => 'redmi',
                'subsubcategory_slug_hin' => 'रेड्मी',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '1',
                'subsubcategory_name_en' => 'Vivo',
                'subsubcategory_name_hin' => 'विवो',
                'subsubcategory_slug_en' => 'vivo',
                'subsubcategory_slug_hin' => 'विवो',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '1',
                'subsubcategory_name_en' => 'OnePlus',
                'subsubcategory_name_hin' => 'वनप्लस',
                'subsubcategory_slug_en' => 'oneplus',
                'subsubcategory_slug_hin' => 'वनप्लस',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '2',
                'subsubcategory_name_en' => 'Dell ',
                'subsubcategory_name_hin' => 'डेल',
                'subsubcategory_slug_en' => 'dell',
                'subsubcategory_slug_hin' => 'डेल',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '2',
                'subsubcategory_name_en' => 'HP',
                'subsubcategory_name_hin' => 'एचपी',
                'subsubcategory_slug_en' => 'hp',
                'subsubcategory_slug_hin' => 'एचपी',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '2',
                'subsubcategory_name_en' => 'Lenovo',
                'subsubcategory_name_hin' => 'लेनोवो',
                'subsubcategory_slug_en' => 'lenovo',
                'subsubcategory_slug_hin' => 'लेनोवो',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '3',
                'subsubcategory_name_en' => 'Acer Tablets',
                'subsubcategory_name_hin' => 'एसर टैबलेट',
                'subsubcategory_slug_en' => 'acer-tablets',
                'subsubcategory_slug_hin' => 'एसर टैबलेट',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '3',
                'subsubcategory_name_en' => 'Apple Tablets',
                'subsubcategory_name_hin' => 'एप्पल-टैबलेट',
                'subsubcategory_slug_en' => 'apple-tablets',
                'subsubcategory_slug_hin' => 'एप्पल-टैबलेट',
            ],
            [
                'category_id' => '1',
                'sub_category_id' => '3',
                'subsubcategory_name_en' => 'Samsung Tablets',
                'subsubcategory_name_hin' => 'सैमसंग-टैबलेट',
                'subsubcategory_slug_en' => 'samsung-tablets',
                'subsubcategory_slug_hin' => 'सैमसंग-टैबलेट',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '4',
                'subsubcategory_name_en' => 'Tshirt',
                'subsubcategory_name_hin' => 'टी शर्ट',
                'subsubcategory_slug_en' => 'tshirt',
                'subsubcategory_slug_hin' => 'टी-शर्ट',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '4',
                'subsubcategory_name_en' => 'Formal Shirts',
                'subsubcategory_name_hin' => 'औपचारिक कमीज़',
                'subsubcategory_slug_en' => 'formal-shirts',
                'subsubcategory_slug_hin' => 'औपचारिक -कमीज़',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '4',
                'subsubcategory_name_en' => 'Casual Shirts',
                'subsubcategory_name_hin' => 'अनौपचारिक कमीज़',
                'subsubcategory_slug_en' => 'Casual-shirts',
                'subsubcategory_slug_hin' => 'अनौपचारिक कमीज़',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '5',
                'subsubcategory_name_en' => 'Jeans',
                'subsubcategory_name_hin' => 'जीन्स',
                'subsubcategory_slug_en' => 'jeans',
                'subsubcategory_slug_hin' => 'जीन्स',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '5',
                'subsubcategory_name_en' => 'Casual Trousers',
                'subsubcategory_name_hin' => 'औपचारिक पतलून',
                'subsubcategory_slug_en' => 'casual-trousers',
                'subsubcategory_slug_hin' => 'औपचारिक-पतलून',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '5',
                'subsubcategory_name_en' => 'Formal Trousers',
                'subsubcategory_name_hin' => 'औपचारिक पतलून',
                'subsubcategory_slug_en' => 'formal-trousers',
                'subsubcategory_slug_hin' => 'औपचारिक-पतलून',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '5',
                'subsubcategory_name_en' => 'Track Pants',
                'subsubcategory_name_hin' => 'ट्रैक पैंट',
                'subsubcategory_slug_en' => 'track-pants',
                'subsubcategory_slug_hin' => 'ट्रैक-पैंट',
            ],
            //

            [
                'category_id' => '2',
                'sub_category_id' => '6',
                'subsubcategory_name_en' => 'Sarees',
                'subsubcategory_name_hin' => 'साड़ियों',
                'subsubcategory_slug_en' => 'sarees',
                'subsubcategory_slug_hin' => 'साड़ियों',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '6',
                'subsubcategory_name_en' => 'Kurtas Kurtis',
                'subsubcategory_name_hin' => 'कुर्ता कुर्तियां',
                'subsubcategory_slug_en' => 'kurtas-kurtis',
                'subsubcategory_slug_hin' => 'कुर्ता-कुर्तियां',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '6',
                'subsubcategory_name_en' => 'Lehenga Cholis',
                'subsubcategory_name_hin' => 'लहंगा चोली',
                'subsubcategory_slug_en' => 'lehenga-cholis',
                'subsubcategory_slug_hin' => 'लहंगा-चोली',
            ],

            [
                'category_id' => '2',
                'sub_category_id' => '7',
                'subsubcategory_name_en' => 'Leggings And Churidars',
                'subsubcategory_name_hin' => 'लेगिंग्स और चूड़ीदार',
                'subsubcategory_slug_en' => 'leggings-and-churidars',
                'subsubcategory_slug_hin' => 'लेगिंग्स-और-चूड़ीदार',
            ],
            [
                'category_id' => '2',
                'sub_category_id' => '7',
                'subsubcategory_name_en' => 'Palazzos',
                'subsubcategory_name_hin' => 'पलाज़ोस',
                'subsubcategory_slug_en' => 'palazzos',
                'subsubcategory_slug_hin' => 'पलाज़ोस',
            ],

        ]);
        collect($subsubcategories)->each(function ($subsubcategory) {
            SubSubCategory::create($subsubcategory);
        });
    }
}
