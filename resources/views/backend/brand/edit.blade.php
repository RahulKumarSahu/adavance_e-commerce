@extends('admin.admin_master')
@section('admin')

<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title">Edit Brand </h3>
                <a href="{{ url('/brand/view') }}" class="btn btn-danger" style="float: right"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <form action="{{ route('brand.update',$brand->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                                <input type="hidden" name="id" value="{{ $brand->id }}">
                                <input type="hidden" name="old_image" value="{{ $brand->brand_image }}">

                                    <div class="form-group">
                                        <h5>Brnad Name English<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="brand_name_en" class="form-control" value="{{ $brand->brand_name_en }}">
                                            @error('brand_name_en')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Brnad Name Hindi<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="brand_name_hin" class="form-control" value="{{ $brand->brand_name_hin }}">
                                            @error('brand_name_hin')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Brand Image<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" name="brand_image" class="form-control">
                                            <img src="{{ asset($brand->brand_image) }}" style="width: 100px; height:100px">
                                            @error('brand_image')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                        </div>
                                    </div>
                           <div class="text-xs-right">
                               <button type="submit" class="btn btn-rounded btn-primary" value="Update">Update</button>
                           </div>
                       </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

@endsection
