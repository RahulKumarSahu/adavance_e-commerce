@extends('admin.admin_master')
@section('admin')

<div class="container-full">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit category </h3>
                        <a href="{{ url('/category/view') }}" class="btn btn-danger" style="float: right"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <form action="{{ route('category.update',$category->id) }}" method="POST">
                                @csrf

                                <input type="hidden" name="id" value="{{ $category->id }}">
                                {{-- <input type="hidden" name="old_image" value="{{ $category->category_image }}"> --}}
                                <div class="form-group">
                                    <h5>Brnad Name English<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="category_name_en" class="form-control"
                                            value="{{ $category->category_name_en }}">
                                        @error('category_name_en')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>Brnad Name Hindi<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="category_name_hin" class="form-control"
                                            value="{{ $category->category_name_hin }}">
                                        @error('category_name_hin')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>category Image<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="category_icon" class="form-control"
                                            value="{{ $category->category_icon }}">
                                        @error('category_icon')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-rounded btn-primary"
                                        value="Update">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

@endsection
