<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Invoice</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .gray {
            background-color: lightgray
        }

        .font {
            font-size: 15px;
        }

        .authority {
            /*text-align: center;*/
            float: right
        }

        .authority h5 {
            margin-top: -10px;
            color: green;
            /*text-align: center;*/
            margin-left: 35px;
        }

        .thanks p {
            color: green;
            ;
            font-size: 16px;
            font-weight: normal;
            font-family: serif;
            margin-top: 20px;
        }
    </style>

</head>

<body>

    <table width="100%" style="background: #F7F7F7; padding:0 20px 0 20px;">
        <tr>
            <td valign="top">
                <!-- {{-- <img src="" alt="" width="150"/> --}} -->
                <h2 style="color: green; font-size: 26px;"><strong>EasyShop</strong></h2>
            </td>
            <td align="right">
                <pre class="font">
               EasyShop Head Office
               Email:support@itsrahulkumarsahu.in <br>
               Mob: 1245454545 <br>
               Mahasamund 493445,Chhattisgarh:#4 <br>

            </pre>
            </td>
        </tr>

    </table>


    <table width="100%" style="background:white; padding:2px;""></table>

  <table width=" 100%" style="background: #F7F7F7; padding:0 5 0 5px;" class="font">
        <tr>
            <td>
                <p class="font" style="margin-left: 20px;">
                    <strong>Name:</strong> {{ $orders->name }} <br>
                    <strong>Email:</strong> {{ $orders->email }} <br>
                    <strong>Phone:</strong> {{ $orders->phone }} <br>

                    @php
                    $div = $orders->division->division_name;
                    $dist = $orders->district->district_name;
                    $state = $orders->state->state_name;
                    @endphp

                    <strong>Address:</strong> {{ $div }},{{ $dist }},{{ $state }} <br>
                    <strong>Post Code:</strong> {{ $orders->post_code }}
                </p>
            </td>
            <td>
                <p class="font">
                <h3><span style="color: green;">Invoice:</span> #{{ $orders->invoice_no }}</h3>
                Order Date: {{ $orders->order_date }} <br>
                Delivery Date: {{ $orders->deliverd_date }} <br>
                Payment Type : {{ $orders->payment_type }} </span>
                </p>
            </td>
        </tr>
    </table>
    <br />
    <h3>Products</h3>


    <table width="100%">
        <thead style="background-color: green; color:#FFFFFF;">
            <tr class="font">
                <th>Image</th>
                <th>Product Name</th>
                <th>Size</th>
                <th>Color</th>
                <th>Code</th>
                <th>Quantity</th>
                <th>Unit Price </th>
                <th>Total </th>
            </tr>
        </thead>
        <tbody>

            @foreach ($orderItem as $item)

            <tr class="font">
                <td align="center">
                    {{ public_path($item->product->product_thambnail) }}
                    <img src="{{ public_path($item->product->product_thambnail) }}" height="60px;" width="60px;" alt="">
                </td>
                <td align="center">{{ $item->product->product_name_en }}</td>
                <td align="center">{{ $item->size }}

                </td>
                <td align="center">{{ $item->color }}</td>
                <td align="center">{{ $item->product->product_code }}</td>
                <td align="center">{{ $item->qty }}</td>
                <td align="center">{{ $item->price }}</td>
                <td align="center">{{ $item->price * $item->qty }}</td>
            </tr>
            @endforeach

        </tbody>
    </table>
    <br>
    <table width="100%" style=" padding:0 10px 0 10px;">
        <tr>
            <td align="right">
                <h2><span style="color: green;">Subtotal:</span>{{ $orders->amount }}</h2>
                <h2><span style="color: green;">Total:</span>{{ $orders->amount }}</h2>
                {{-- <h2><span style="color: green;">Full Payment PAID</h2> --}}
            </td>
        </tr>
    </table>
    <div class="thanks mt-3">
        <p>Thanks For Buying Products..!!</p>
    </div>
    <div class="authority float-right mt-5">
        <p>-----------------------------------</p>
        <h5>Authority Signature:</h5>
    </div>
</body>

</html>
