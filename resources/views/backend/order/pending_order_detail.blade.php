@extends('admin.admin_master')
@section('admin')


<!-- Content Wrapper. Contains page content -->

<div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Order Details</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                            <li class="breadcrumb-item" aria-current="page">Order Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <!-- Main content -->
    <section class="content">
        <div class="row">



            <div class="col-md-6 col-12">
                <div class="box box-bordered border-primary">
                    <div class="box-header with-border">
                        <h4 class="box-title"><strong>Shipping Details</strong></h4>
                    </div>
                    <table class="table">

                        <tr>
                            <th>Shipping Name :</th>
                            <th> {{ $orders->name }} </th>
                        </tr>
                        <tr>
                            <th>Shipping Email :</th>
                            <th>{{ $orders->email }} </th>
                        </tr>
                        <tr>
                            <th>Division :</th>
                            <th>{{ $orders->division->division_name }}</th>
                        </tr>
                        <tr>
                            <th>District :</th>
                            <th>{{ $orders->district->district_name }}</th>
                        </tr>
                        <tr>
                            <th>State :</th>
                            <th>{{ $orders->state->state_name }}</th>
                        </tr>
                        <tr>
                            <th>Post Code :</th>
                            <th>{{ $orders->post_code }}</th>
                        </tr>
                        <tr>
                            <th>Order Date :</th>
                            <th>{{ $orders->order_date }}</th>
                        </tr>

                    </table>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-6 col-12">
                <div class="box box-bordered border-primary">
                    <div class="box-header with-border">
                        <h4 class="box-title"><strong>Order Details</strong>
                            <span class="text-danger">Invoice : {{ $orders->invoice_no }}</span>
                        </h4>
                    </div>
                    <table class="table">

                        <tr>
                            <th>Name :</th>
                            <th> {{ $orders->name }} </th>
                        </tr>
                        <tr>
                            <th>Phone :</th>
                            <th>{{ $orders->phone }} </th>
                        </tr>
                        <tr>
                            <th>Payment Type :</th>
                            <th>{{ $orders->payment_type }}</th>
                        </tr>
                        <tr>
                            <th>Trans ID :</th>
                            <th>{{ $orders->transaction_id }}</th>
                        </tr>
                        <tr>
                            <th>Invoice :</th>
                            <th class="text-danger">{{ $orders->invoice_no }}</th>
                        </tr>
                        <tr>
                            <th>Order Total :</th>
                            <th>{{ $orders->amount }}</th>
                        </tr>
                        <tr>
                            <th>Order :</th>
                            <th>
                                <span class="badge badge-pill badge-danger" style="background: #418DB9;">
                                    {{ $orders->status }}
                                </span>
                            </th>
                        </tr>

                        <tr>
                            <th></th>

                            <th>
                                @if ($orders->status == 'pending')
                                <a href="{{ route('pending-confirm',$orders->id) }}" class="btn btn-block btn-success"
                                    id="confirm">Confirm Order</a>

                                @elseif ($orders->status == 'confirmed')
                                <a href="{{ route('confirm-processing',$orders->id) }}"
                                    class="btn btn-block btn-success" id="processing">Processing Order</a>

                                @elseif ($orders->status == 'processing')
                                <a href="{{ route('processing-picked',$orders->id) }}" class="btn btn-block btn-success"
                                    id="picked">Processing Order</a>

                                @elseif ($orders->status == 'picked')
                                <a href="{{ route('picked-shipped',$orders->id) }}" class="btn btn-block btn-success"
                                    id="shipped">Processing Order</a>

                                @elseif ($orders->status == 'shipped')
                                <a href="{{ route('shipped-delivered',$orders->id) }}" class="btn btn-block btn-success"
                                    id="delivered">Delivered Order</a>

                                @elseif ($orders->status == 'delivered')
                                <a href="{{ route('delivered-cancel',$orders->id) }}" class="btn btn-block btn-success"
                                    id="cancel">Cancel Order</a>

                                @endif
                            </th>
                        </tr>


                    </table>
                </div>
            </div>
            <!-- /.col -->

            <div class="col-md-12 col-12">
                <div class="box box-bordered border-primary">

                    <div class="box-header with-border">
                        <h4 class="box-title"><strong></strong>
                        </h4>
                    </div>
                    <table class="table">
                        <tbody>
                            <tr>

                                <td class="col-md-2">
                                    <label for="">Product Image</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">Product</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Product Code</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">Product Color</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Product Size</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Quantity</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Price</label>
                                </td>

                            </tr>

                            @foreach ($orderItem as $item)

                            <tr>

                                <td class="col-md-2">
                                    <label for="">
                                        <img src="{{ asset($item->product->product_thambnail) }}" width="50px">
                                    </label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">{{ $item->product->product_name_en }}</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">{{ $item->product->product_code }}</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">{{ $item->color }}</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">{{ $item->size }}</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">{{ $item->qty }}</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">$ {{ $item->price }} ({{ $item->price * $item->qty }})</label>
                                </td>
                            </tr>

                            @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
            <!-- /.col -->


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

</div>

@section('scrpit')
<script src="{{ asset('../assets/vendor_components/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('backend/js/pages/data-table.js') }}"></script>
@endsection


@endsection
