@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="container-full">

    <section class="content">

        <!-- Basic Forms -->
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Edit Admin User</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col">
                        <form action="{{ route('update.admin.user') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <input type="hidden" name="id" value="{{ $adminUser->id }}">
                            <input type="hidden" name="old_image" value="{{ $adminUser->profile_photo_path }}">

                            <div class="row">
                                <div class="col-12">

                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Admin User Name<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="name" class="form-control"
                                                        value="{{ $adminUser->name }}">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Admin Email<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="email" name="email" class="form-control"
                                                        value="{{ $adminUser->email }}">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Admin User Phone<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="phone" class="form-control"
                                                        value="{{ $adminUser->phone }}">
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <h5>Admin User Image <span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="file" id="image" name="profile_photo_path"
                                                        class="form-control">
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-6">
                                            <img src="{{ $adminUser->profile_photo_path }}" id="showImage"
                                                style="width: 100px" height="100px">

                                        </div>
                                    </div>


                                    <hr>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_2" name="brand" value="1" {{
                                                            $adminUser->brand == 1 ? 'checked' : '' }} >
                                                        <label for="checkbox_2">Brand</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_3" name="category" value="1"
                                                            {{ $adminUser->category == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_3">Category</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_4" name="product" value="1"
                                                            {{ $adminUser->product == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_4">Product</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_5" name="slider" value="1"
                                                            {{ $adminUser->slider == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_5">Slider</label>
                                                    </fieldset>

                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_6" name="coupons" value="1"
                                                            {{ $adminUser->coupons == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_6">Coupons</label>
                                                    </fieldset>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_7" name="shipping" value="1"
                                                            {{ $adminUser->shipping == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_7">Shipping</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_8" name="orders" value="1"
                                                            {{ $adminUser->orders == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_8">Orders</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_9" name="reports" value="1"
                                                            {{ $adminUser->reports == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_9">Reports</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_10" name="alluser" value="1"
                                                            {{ $adminUser->alluser == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_10">Alluser</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_11" name="setting" value="1"
                                                            {{ $adminUser->setting == 1 ? 'checked' : '' }}>
                                                        <label for="checkbox_11">Setting</label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <div class="controls">
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_12" name="order_return"
                                                            value="1" {{ $adminUser->order_return == 1 ? 'checked' : ''
                                                        }}>
                                                        <label for="checkbox_12">Order Return</label>
                                                    </fieldset>
                                                    <fieldset>
                                                        <input type="checkbox" id="checkbox_13" name="admin_user_role"
                                                            value="1" {{ $adminUser->admin_user_role == 1 ? 'checked' :
                                                        '' }}>
                                                        <label for="checkbox_13">Admin User Role</label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="text-xs-right">
                                        <button type="submit" class="btn btn-rounded btn-primary">Update Admin
                                            User</button>
                                    </div>
                        </form>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
</div>
<script type="text/javascript">
    $(document).ready(function(){
            $('#image').change(function(e){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#showImage').attr('src',e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
</script>
@endsection
