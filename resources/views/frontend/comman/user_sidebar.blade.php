@php
$id = Auth::user()->id;
$user =App\Models\User::find($id);
@endphp

<div class="col-md-2"><br>
    <img class="card-img-top" style="border-radius: 50%" src="{{ $user->profile_image }}" height="100%"
        width="100%"><br><br>
    <ul class="list-group list-group-flush">

        <a href="{{ url('dashboard') }}" class="btn btn-primary btn-sm btn-block">Home</a>

        <a href="{{ route('user.profile') }}" class="btn btn-primary btn-sm btn-block">Profile Update</a>

        <a href="{{ route('change.password') }}" class="btn btn-primary btn-sm btn-block">Change
            Password</a>

        <a href="{{ route('my.order') }}" class="btn btn-primary btn-sm btn-block">My Order</a>

        <a href="{{ route('return.order.list') }}" class="btn btn-primary btn-sm btn-block">Return Order</a>

        <a href="{{ route('cancel.order.list') }}" class="btn btn-primary btn-sm btn-block">Cancel Order</a>

        <a href="{{ route('user.logout') }}" class="btn btn-danger btn-sm btn-block">Logout</a>

    </ul>
</div>
