{{-- <ul>
    @foreach ($products as $item)
    <li>
        <img src="{{ asset($item->product_thambnail) }}" style="width: 30px;">
        {{ $item->product_name_en }}
    </li>

    @endforeach
</ul> --}}
@if ($products->isEmpty())
<h3 class="text-center text-danger">Product Not Found</h3>

@else


<div class="container mt-5">
    <div class="row d-flex justify-content-center ">
        <div class="col-md-6">
            <div class="card">

                @foreach ($products as $item)
                <a href="{{ url('product/detail/'.$item->id.'/'.$item->product_slug_en) }}">
                    <div class="list border-bottom">
                        <img src="{{ asset($item->product_thambnail) }}" class="thamb">
                        <div class="d-flex flex-column ml-3" id="name"> <span>
                                {{ $item->product_name_en }}</span> <small>${{ $item->selling_price }}</small> </div>
                    </div>
                </a>

                @endforeach

            </div>
        </div>
    </div>
</div>
@endif


<link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css')}}">
