@extends('frontend.main_master')
@section('content')

@section('title')
{{ $product->product_name_en }}
@endsection


<!-- ===== ======== HEADER : END ============================================== -->
<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled">
                <li><a href="#">Home</a></li>
                <li><a href="#">Clothing</a></li>
                <li class='active'>Floral Print Buttoned</li>
            </ul>
        </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
    <div class='container'>
        <div class='row single-product'>
            <div class='col-md-3 sidebar'>
                <div class="sidebar-module-container">
                    <div class="home-banner outer-top-n">
                        <img src="{{ asset('frontend/assets/images/banners/LHS-banner.jpg') }}" alt="Image">
                    </div>



                    <!-- ============================================== HOT DEALS ============================================== -->
                    @include('frontend.comman.hot_deals')
                    <!-- ============================================== HOT DEALS: END ============================================== -->

                    <!-- ============================================== NEWSLETTER ============================================== -->
                    <div class="sidebar-widget newsletter wow fadeInUp outer-bottom-small outer-top-vs">
                        <h3 class="section-title">Newsletters</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <p>Sign Up for Our Newsletter!</p>
                            <form>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                        placeholder="Subscribe to our newsletter">
                                </div>
                                <button class="btn btn-primary">Subscribe</button>
                            </form>
                        </div><!-- /.sidebar-widget-body -->
                    </div><!-- /.sidebar-widget -->
                    <!-- ============================================== NEWSLETTER: END ============================================== -->


                </div>
            </div><!-- /.sidebar -->
            <div class='col-md-9'>
                <div class="detail-block">
                    <div class="row  wow fadeInUp">

                        <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                            <div class="product-item-holder size-big single-product-gallery small-gallery">

                                <div id="owl-single-product">
                                    @foreach ($multiImg as $img)

                                    <div class="single-product-gallery-item" id="slide{{ $img->id }}">
                                        <a data-lightbox="image-1" data-title="Gallery"
                                            href="{{ asset($img->photo_name) }}">
                                            <img class="img-responsive" alt="" src="{{ asset($img->photo_name) }}"
                                                data-echo="{{ asset($img->photo_name) }}" />

                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    @endforeach



                                </div><!-- /.single-product-slider -->


                                <div class="single-product-gallery-thumbs gallery-thumbs">

                                    <div id="owl-single-product-thumbnails">
                                        @foreach ($multiImg as $img)

                                        <div class="item">
                                            <a class="horizontal-thumb active" data-target="#owl-single-product"
                                                data-slide="1" href="#slide{{ $img->id}}">
                                                <img class="img-responsive" width="85" alt=""
                                                    src="{{ asset($img->multi_image) }}"
                                                    data-echo="{{ asset($img->multi_image) }}" />
                                            </a>
                                        </div>
                                        @endforeach

                                    </div><!-- /#owl-single-product-thumbnails -->



                                </div><!-- /.gallery-thumbs -->

                            </div><!-- /.single-product-gallery -->
                        </div><!-- /.gallery-holder -->
                        <div class='col-sm-6 col-md-7 product-info-block'>
                            <div class="product-info">
                                <h1 class="name">
                                    @if (session()->get('language') == 'hindi') {{
                                    $product->product_name_hin}} @else {{
                                    $product->product_name_en}}
                                    @endif
                                </h1>

                                {{-- <div class="rating-reviews m-t-20">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="rating rateit-small"></div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="reviews">
                                                <a href="#" class="lnk">(13 Reviews)</a>
                                            </div>
                                        </div>
                                    </div><!-- /.row -->
                                </div><!-- /.rating-reviews --> --}}

                                <div class="stock-container info-container m-t-10">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="stock-box">
                                                <span class="label">Availability :</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="stock-box">
                                                <span class="value">In Stock</span>
                                            </div>
                                        </div>
                                    </div><!-- /.row -->
                                </div><!-- /.stock-container -->

                                <div class="description-container m-t-20">
                                    @if (session()->get('language') == 'hindi') {{
                                    $product->short_descp_hin}} @else {{
                                    $product->short_descp_en}}
                                    @endif
                                </div><!-- /.description-container -->

                                <div class="price-container info-container m-t-20">
                                    <div class="row">


                                        <div class="col-sm-6">
                                            <div class="price-box">

                                                @if ($product->discount_price == NULL)
                                                <span class="price">${{
                                                    $product->selling_price }}</span>
                                                @else
                                                <span class="price">${{
                                                    $product->discount_price }}</span>

                                                <span class="price-strike">${{
                                                    $product->selling_price }}</span>

                                                @endif

                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="favorite-button m-t-10">
                                                <a class="btn btn-primary" data-toggle="tooltip" data-placement="right"
                                                    title="Wishlist" href="#">
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>

                                    </div><!-- /.row -->
                                </div><!-- /.price-container -->


                                {{-- //add product color and product size --}}

                                <div class="row">


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="info-title control-label">Choose Color <span>*</span></label>
                                            <select class="form-control unicase-form-control selectpicker">
                                                <option selected="" disabled="">--Select options--</option>
                                                @foreach ($product_color_en as $color)

                                                <option value="{{ $color }}">{{ ucwords($color) }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- end form group --}}

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="info-title control-label">Choose Size <span>*</span></label>
                                            <select class="form-control unicase-form-control selectpicker">
                                                <option selected="" disabled="">--Select options--</option>
                                                @foreach ($product_size_en as $size)

                                                <option value="{{ $size }}">{{ ucwords($size) }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div><!-- /.row -->
                                {{-- //end product color and product size --}}


                                <di3v class="quantity-container info-container">
                                    <div class="row">

                                        <div class="col-sm-2">
                                            <span class="label">Qty :</span>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="cart-quantity">
                                                <div class="quant-input">
                                                    <div class="arrows">
                                                        <div class="arrow plus gradient"><span class="ir"><i
                                                                    class="icon fa fa-sort-asc"></i></span></div>
                                                        <div class="arrow minus gradient"><span class="ir"><i
                                                                    class="icon fa fa-sort-desc"></i></span></div>
                                                    </div>
                                                    <input type="text" value="1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-7">
                                            <input type="hidden" id="product_id">
                                            <button type="submit" class="btn btn-primary mb-2" onclick="addToCart()">Add
                                                to
                                                Cart</button>
                                            {{-- <a href="#" class="btn btn-primary" onclick="addToCart()>
                                                <i class= " fa fa-shopping-cart inner-right-vs"></i> ADD TO CART</a>
                                            --}}

                                        </div>


                                    </div><!-- /.row -->
                                </di3v><!-- /.quantity-container -->






                            </div><!-- /.product-info -->
                        </div><!-- /.col-sm-7 -->
                    </div><!-- /.row -->
                </div>

                <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                                {{-- <li><a data-toggle="tab" href="#review">REVIEW</a></li>
                                <li><a data-toggle="tab" href="#tags">TAGS</a></li> --}}
                            </ul><!-- /.nav-tabs #product-tabs -->
                        </div>
                        <div class="col-sm-9">

                            <div class="tab-content">

                                <div id="description" class="tab-pane in active">
                                    <div class="product-tab">
                                        <p class="text">
                                            @if (session()->get('language') == 'hindi') {{
                                            $product->long_descp_hin }} @else {{
                                            $product->long_descp_en }}
                                            @endif
                                        </p>
                                    </div>
                                </div><!-- /.tab-pane -->


                            </div><!-- /.tab-content -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.product-tabs -->

                <!-- ============================================== UPSELL PRODUCTS ============================================== -->
                <section class="section featured-product wow fadeInUp">
                    <h3 class="section-title">upsell products</h3>
                    <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">

                        @foreach ( $relateProduct as $product )

                        <div class="item item-carousel">
                            <div class="products">

                                <div class="product">
                                    <div class="product-image">
                                        <div class="image">
                                            <a
                                                href="{{ url('product/detail/'.$product->id.'/'.$product->product_slug_en) }}">
                                                <img src="{{ asset( $product->product_thambnail )}}" alt=""></a>
                                        </div><!-- /.image -->

                                        {{-- <div class="tag sale"><span>sale</span></div> --}}
                                        @php
                                        $amount = $product->selling_price - $product->discount_price;
                                        $discount =($amount / $product->selling_price) * 100;
                                        @endphp

                                        <div class="tag sale"><span>sale</span></div>
                                    </div>

                                    <div class="product-info text-left">
                                        <h3 class="name"><a
                                                href="{{ url('product/detail/'.$product->id.'/'.$product->product_slug_en) }}">
                                                @if (session()->get('language') == 'hindi') {{
                                                $product->product_name_hin
                                                }} @else {{ $product->product_name_en
                                                }}
                                                @endif
                                            </a></h3>
                                        {{-- <div class="rating rateit-small"></div>
                                        <div class="description"></div> --}}

                                        @if ($product->discount_price == NULL)

                                        <div class="product-price"> <span class="price"> ${{
                                                $product->selling_price }} </span>
                                        </div>

                                        @else
                                        <div class="product-price"> <span class="price"> ${{
                                                $product->discount_price }} </span>
                                            <span class="price-before-discount">${{
                                                $product->selling_price }}</span>
                                        </div>
                                        @endif
                                        <!-- /.product-price -->

                                    </div><!-- /.product-info -->
                                    <div class="cart clearfix animate-effect">
                                        <div class="action">
                                            <ul class="list-unstyled">
                                                <li class="add-cart-button btn-group">
                                                    <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </button>
                                                    <button class="btn btn-primary cart-btn" type="button">Add to
                                                        cart</button>

                                                </li>

                                                <li class="lnk wishlist">
                                                    <a class="add-to-cart" href="detail.html" title="Wishlist">
                                                        <i class="icon fa fa-heart"></i>
                                                    </a>
                                                </li>

                                                <li class="lnk">
                                                    <a class="add-to-cart" href="detail.html" title="Compare">
                                                        <i class="fa fa-signal"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div><!-- /.action -->
                                    </div><!-- /.cart -->
                                </div><!-- /.product -->

                            </div><!-- /.products -->
                        </div><!-- /.item -->

                        @endforeach

                    </div><!-- /.home-owl-carousel -->
                </section><!-- /.section -->
                <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->

            </div><!-- /.col -->
            <div class="clearfix"></div>
        </div><!-- /.row -->
    </div>
    @include('frontend.body.brands')


    @endsection
    <script>
        // Start add to cart product

function addToCart(){
            var product_name = $('#pname').text();
            var id = $('#product_id').val();
            var color = $('#color option:selected').text();
            var size = $('#size option:selected').text();
            var quantity = $('#qty').val();
            $.ajax({
                type: "POST",
                dataType: 'json',
                data:{
                    color:color,size:size,quantity:quantity,product_name:product_name
                },
                url: "/cart/data/store/" +id,
                success:function(data){
                    miniCart()
                    $('#closeModel').click();
                    // console.log(data);

                    //start messsage

                    const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 3000
                            })
                            if ($.isEmptyObject(data.error)) {
                                Toast.fire({
                                    type: 'success',
                                    title:data.success
                                })
                            } else {
                                Toast.fire({
                                    type: 'error',
                                    title:data.error
                                })
                            }
                    //end messsage

                }
            })

        }
    </script>
