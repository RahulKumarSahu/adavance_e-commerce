@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">

            @include('frontend.comman.user_sidebar')

            <div class="col-md-2">
            </div>

            <div class="col-md-6">
                <div class="card">
                    <h3 class="text-center"><span class="text-danger">Hi..</span> <strong>
                        {{ Auth::user()->name }} </strong> Update Your Profile</h3>
                        <div class="card-body">
                            <form action="{{ route('user.profile.store') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group">
                                    <label class="info-title" for="name">Name <span>*</span></label>
                                    <input type="text" name="name" class="form-control unicase-form-control text-input" id="name"value="{{ $user->name }}" >
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="email">Email <span>*</span></label>
                                    <input type="email" name="email" class="form-control unicase-form-control text-input" id="email" value="{{ $user->email }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="phone">Phone Number<span>*</span></label>
                                    <input type="text" name="phone" class="form-control unicase-form-control text-input" id="phone" value="{{ $user->phone }}">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="profile_photo_path">User Profile<span>*</span></label>
                                    <input type="file" name="profile_photo_path" class="form-control unicase-form-control text-input" id="profile_photo_path">
                                    @error('profile_photo_path')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
	  	                    <button type="submit" class="btn btn-primary" style="margin-bottom: 10px">Update</button>

                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
