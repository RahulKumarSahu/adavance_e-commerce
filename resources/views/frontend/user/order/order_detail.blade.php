@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">

            @include('frontend.comman.user_sidebar')



            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Shipping Details</h4>
                    </div>
                    <hr>
                    <div class="card-body" style="background:rgb(231, 230, 230);">
                        <table class="table">

                            <tr>
                                <th>Shipping Name :</th>
                                <th> {{ $orders->name }} </th>
                            </tr>
                            <tr>
                                <th>Shipping Email :</th>
                                <th>{{ $orders->email }} </th>
                            </tr>
                            <tr>
                                <th>Division :</th>
                                <th>{{ $orders->division->division_name }}</th>
                            </tr>
                            <tr>
                                <th>District :</th>
                                <th>{{ $orders->district->district_name }}</th>
                            </tr>
                            <tr>
                                <th>State :</th>
                                <th>{{ $orders->state->state_name }}</th>
                            </tr>
                            <tr>
                                <th>Post Code :</th>
                                <th>{{ $orders->post_code }}</th>
                            </tr>
                            <tr>
                                <th>Order Date :</th>
                                <th>{{ $orders->order_date }}</th>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h4>Order Details
                            <span class="text-danger">Invoice : {{ $orders->invoice_no }}</span>
                        </h4>
                    </div>
                    <hr>
                    <div class="card-body" style="background:rgb(231, 230, 230);">
                        <table class="table">

                            <tr>
                                <th>Name :</th>
                                <th> {{ $orders->name }} </th>
                            </tr>
                            <tr>
                                <th>Phone :</th>
                                <th>{{ $orders->phone }} </th>
                            </tr>
                            <tr>
                                <th>Payment Type :</th>
                                <th>{{ $orders->payment_type }}</th>
                            </tr>
                            <tr>
                                <th>Trans ID :</th>
                                <th>{{ $orders->transaction_id }}</th>
                            </tr>
                            <tr>
                                <th>Invoice :</th>
                                <th class="text-danger">{{ $orders->invoice_no }}</th>
                            </tr>
                            <tr>
                                <th>Order Total :</th>
                                <th>{{ $orders->amount }}</th>
                            </tr>
                            <tr>
                                <th>Order :</th>
                                <th>
                                    <span class="badge badge-pill badge-danger" style="background: #418DB9;">
                                        {{ $orders->status }}
                                    </span>
                                </th>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr style="background: #e2e2e2;">

                                <td class="col-md-2">
                                    <label for="">Product Image</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">Product</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Product Code</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">Product Color</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Product Size</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Quantity</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">Price</label>
                                </td>

                            </tr>

                            @foreach ($orderItem as $item)

                            <tr>

                                <td class="col-md-2">
                                    <label for="">

                                        <img src="{{ asset($item->product->product_thambnail) }}" width="50px">
                                    </label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">{{ $item->product->product_name_en }}</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">{{ $item->product->product_code }}</label>
                                </td>

                                <td class="col-md-2">
                                    <label for="">{{ $item->color }}</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">{{ $item->size }}</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">{{ $item->qty }}</label>
                                </td>
                                <td class="col-md-2">
                                    <label for="">$ {{ $item->price }} $({{ $item->price * $item->qty }})</label>
                                </td>
                            </tr>

                            @endforeach


                        </tbody>

                    </table>
                </div>
            </div>


        </div>




        @if ($orders->status !== "delivered")

        @else

        @php
        $orders = App\Models\Order::where('id',$orders->id)->where('return_reason','=',NULL)->first();
        @endphp

        @if ($orders)

        <form action="{{ route('return.order',$orders->id) }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="">Order Return Reason: </label>
                <textarea name="return_reason" id="" class="form-group" cols="150" rows="05">Return Reason</textarea>
            </div>
            <button class="btn btn-danger"> Return Order </button>

        </form>


        @else
        <span class="badge badge-pill badge-warning" style="background: red">You have send return request for this
            product</span>
        @endif

        @endif
        <br><br>
    </div>
</div>

@endsection
