@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">

            @include('frontend.comman.user_sidebar')

            <div class="col-md-2"></div>

            <div class="col-md-8">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr style="background: #e2e2e2;">

                                <td class="col-md-1">
                                    <label for="">Date</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">Total</label>
                                </td>
                                <td class="col-md-3">
                                    <label for="">Payment</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">Invoice</label>
                                </td>
                                <td class="col-md-3">
                                    <label for="">Order</label>
                                </td>
                                <td class="col-md-1">
                                    <label for="">Action</label>
                                </td>

                            </tr>

                            @foreach ($orders as $order)

                            <tr>

                                <td class="col-md-1">
                                    <label for="">{{ $order->order_date }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">${{ $order->amount }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">{{ $order->payment_method }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">{{ $order->invoice_no }}</label>
                                </td>
                                <td class="col-md-3">

                                    @if ($order->status == 'pending')

                                    <span class="badge badge-pill badge-danger" style="background: #8722a0">
                                        <label for="">Pending</label>
                                    </span>

                                    @elseif ($order->status == 'confirmed')

                                    <span class="badge badge-pill badge-danger" style="background: #1771ad">
                                        <label for="">Confirmed</label>
                                    </span>
                                    @elseif ($order->status == 'processing')

                                    <span class="badge badge-pill badge-danger" style="background: #a8e209">
                                        <label for="">Processing</label>
                                    </span>
                                    @elseif ($order->status == 'picked')

                                    <span class="badge badge-pill badge-danger" style="background: #26d3a8">
                                        <label for="">Picked</label>
                                    </span>
                                    @elseif ($order->status == 'shipped')

                                    <span class="badge badge-pill badge-danger" style="background: #1745dd">
                                        <label for="">Shipped</label>
                                    </span>
                                    @elseif ($order->status == 'delivered')

                                    <span class="badge badge-pill badge-danger" style="background: #66c56ecc">
                                        <label for="">Delivered</label>
                                    </span>
                                    @else

                                    <span class="badge badge-pill badge-danger" style="background: #e42908">
                                        <label for="">Cancel</label>
                                    </span>

                                    @endif

                                </td>
                                <td class="col-md-1">
                                    <a href="{{ url('user/order_details/'.$order->id) }}"
                                        class="btn btn-sm btn-primary"><i class="fa fa-eye"> View</i></a>
                                    <a href="{{ url('user/invoice_downlaod/'.$order->id) }}"
                                        class="btn btn-sm btn-danger" style="margin-top: 5px;" target="_blank"><i
                                            class="fa fa-download">
                                            Invoice</i></a>
                                </td>

                            </tr>

                            @endforeach

                        </tbody>

                    </table>
                </div>
            </div>

        </div>


    </div>
</div>

@endsection
