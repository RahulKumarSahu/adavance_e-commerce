@extends('frontend.main_master')
@section('content')

<div class="body-content">
    <div class="container">
        <div class="row">

            @include('frontend.comman.user_sidebar')

            <div class="col-md-2"></div>

            <div class="col-md-8">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr style="background: #e2e2e2;">

                                <td class="col-md-1">
                                    <label for="">Date</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">Total</label>
                                </td>
                                <td class="col-md-3">
                                    <label for="">Payment</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">Invoice</label>
                                </td>
                                <td class="col-md-3">
                                    <label for="">Return Reason</label>
                                </td>
                                <td class="col-md-1">
                                    <label for="">Order Status</label>
                                </td>

                            </tr>

                            @foreach ($orders as $order)

                            <tr>

                                <td class="col-md-1">
                                    <label for="">{{ $order->order_date }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">${{ $order->amount }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">{{ $order->payment_method }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">{{ $order->invoice_no }}</label>
                                </td>

                                <td class="col-md-3">
                                    <label for="">{{ $order->return_reason }}</label>
                                </td>

                                <td class="col-md-3">

                                    <label for="">

                                        @if ($order->return_order == 0)
                                        <span class="badge badge-pill badge-warning" style="background: #07a2ad">
                                            No Return Requested</span>
                                        @elseif ($order->return_order == 1)
                                        <span class="badge badge-pill badge-warning" style="background: #800000">
                                            Pendding</span>
                                        <span class="badge badge-pill badge-danger" style="background: #f00404">
                                            Return Requested
                                        </span>
                                        @elseif ($order->return_order == 2)
                                        <span class="badge badge-pill badge-warning" style="background:#07ad2b">
                                            Success
                                        </span>

                                        @endif

                                    </label>
                                </td>

                                {{-- <td class=" col-md-1">
                                    <a href="{{ url('user/order_details/'.$order->id) }}"
                                        class="btn btn-sm btn-primary"><i class="fa fa-eye"> View</i></a>
                                    <a href="{{ url('user/invoice_downlaod/'.$order->id) }}"
                                        class="btn btn-sm btn-danger" style="margin-top: 5px;" target="_blank"><i
                                            class="fa fa-download">
                                            Invoice</i></a>
                                </td> --}}

                            </tr>

                            @endforeach

                        </tbody>

                    </table>
                </div>
            </div>

        </div>


    </div>
</div>

@endsection
