<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Backend\AdminProfileController as BackendAdminProfileController;
use App\Http\Controllers\Backend\AdminUserController;
use App\Http\Controllers\Backend\BrandController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\CouponController;
use App\Http\Controllers\Backend\LanguageController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\ReportController;
use App\Http\Controllers\Backend\ReturnController;
use App\Http\Controllers\Backend\ShippingAreaController;
use App\Http\Controllers\Backend\SiteSettingController;
use App\Http\Controllers\Backend\SliderController;
use App\Http\Controllers\Backend\SubCategoryController;
use App\Http\Controllers\Backend\SubSubCategoryController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\IndexController;
use App\Http\Controllers\Frontend\SearchController;
use App\Http\Controllers\User\AllUserController;
use App\Http\Controllers\User\CartPageController;
use App\Http\Controllers\User\CashController;
use App\Http\Controllers\User\CheckoutController;
use App\Http\Controllers\User\StripeController;
use App\Http\Controllers\User\WishlistController;
use App\Models\User;
use Doctrine\DBAL\Schema\Index;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Route::group(['prefix' => 'admin', 'middleware' => ['admin:admin']], function () {

    Route::get('/login', [AdminController::class, 'loginForm']);

    Route::post('/login', [AdminController::class, 'store'])->name('admin.login');
});


//user routes

Route::middleware(['auth:sanctum,web', 'verified'])->get('/dashboard', function () {

    $id = Auth::user()->id;
    $user = User::find($id);

    return view('dashboard', compact('user'));
})->name('dashboard');

Route::get('/', [IndexController::class, 'index'])->name('index');

Route::get('/user.logout', [IndexController::class, 'userLogout'])->name('user.logout');

Route::get('/user.profile', [IndexController::class, 'userProfile'])->name('user.profile');

Route::post('/user.profile.store', [IndexController::class, 'userProfileStore'])
    ->name('user.profile.store');

Route::get('/user.change.password', [IndexController::class, 'userChangePassword'])
    ->name('change.password');

Route::post('/user.password.update', [IndexController::class, 'userUpdatePassword'])
    ->name('user.password.update');

Route::middleware(['auth:admin'])->group(function () {


    Route::middleware(['auth:sanctum,admin', 'verified'])->get('admin/dashboard', function () {
        return view('admin.index');
    })->name('dashboard')->middleware('auth:admin');

    //Admin routes
    Route::get('/logout', [AdminController::class, 'destroy'])->name('admin.logout');

    Route::get('/adminProfile', [BackendAdminProfileController::class, 'adminProfile'])->name('admin.profile');

    Route::get('/adminProfileEdit', [BackendAdminProfileController::class, 'adminProfileEdit'])
        ->name('admin.profile_edit');

    Route::post('/adminProfileStore', [BackendAdminProfileController::class, 'adminProfileStore'])
        ->name('admin.profile.store');

    Route::get('/adminChangePassword', [BackendAdminProfileController::class, 'adminChangePassword'])
        ->name('admin.change.password');

    Route::post('/updateChangePassword', [BackendAdminProfileController::class, 'updateChangePassword'])
        ->name('update.change.password');
}); // end admin middleware

//Brands all route

Route::prefix('brand')->group(function () {

    Route::get('/view', [BrandController::class, 'brandView'])->name('all.brands');

    Route::post('/store', [BrandController::class, 'brandStore'])->name('brand.store');

    Route::get('/edit/{id}', [BrandController::class, 'brandEdit'])->name('brand.edit');

    Route::post('/update/{id}', [BrandController::class, 'brandUpdate'])->name('brand.update');

    Route::get('/delete/{id}', [BrandController::class, 'brandDelete'])->name('brand.delete');
});

//Admin Category all route

Route::prefix('category')->group(function () {

    Route::get('/view', [CategoryController::class, 'categoryView'])->name('all.category');

    Route::post('/store', [CategoryController::class, 'categoryStore'])->name('category.store');

    Route::get('/edit/{id}', [CategoryController::class, 'categoryEdit'])->name('category.edit');

    Route::post('/update/{id}', [CategoryController::class, 'categoryUpdate'])->name('category.update');

    Route::get('/delete/{id}', [CategoryController::class, 'categoryDelete'])->name('category.delete');

    //Admin Sub Category all route

    Route::get('/sub/view', [SubCategoryController::class, 'index'])->name('all.subcategory');

    Route::post('/sub/store', [SubCategoryController::class, 'store'])->name('subcategory.store');

    Route::get('/sub/edit/{id}', [SubCategoryController::class, 'edit'])->name('subcategory.edit');

    Route::post('/sub/update/', [SubCategoryController::class, 'update'])->name('subcategory.update');

    Route::get('/sub/delete/{id}', [SubCategoryController::class, 'delete'])->name('subcategory.delete');

    //Admin Sub Category all route

    Route::get('/sub/sub/view', [SubSubCategoryController::class, 'index'])->name('all.subsubcategory');

    Route::get('/subcategory/ajax/{category_id}', [SubSubCategoryController::class, 'getSubCategory'])
        ->name('getSubCategory');

    Route::get('/sub-subcategory/ajax/{subcategory_id}', [SubSubCategoryController::class, 'GetSubSubCategory']);

    Route::post('/sub/sub/store', [SubSubCategoryController::class, 'store'])->name('subsubcategory.store');

    Route::get('/sub/sub/edit/{id}', [SubSubCategoryController::class, 'edit'])->name('subsubcategory.edit');

    Route::post('/sub/sub/update/', [SubSubCategoryController::class, 'update'])->name('subsubcategory.update');

    Route::get('/sub/sub/delete/{id}', [SubSubCategoryController::class, 'delete'])->name('subsubcategory.delete');
});

Route::prefix('product')->group(function () {

    Route::get('/add', [ProductController::class, 'create'])->name('add.product');

    Route::post('/store', [ProductController::class, 'store'])->name('product.store');

    Route::get('/manage', [ProductController::class, 'index'])->name('manage.product');

    Route::get('/show/{id}', [ProductController::class, 'show'])->name('product.view');

    Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');

    Route::post('/update', [ProductController::class, 'update'])->name('product.update');

    Route::post('/multi/image', [ProductController::class, 'updateMultiImg'])->name('update.image');

    Route::post('/thambnail/image', [ProductController::class, 'updateThamImg'])->name('update.image');

    Route::get('/multiImg/delete/{id}', [ProductController::class, 'multiImgDel'])->name('product.multiImgDel');

    Route::get('/active/{id}', [ProductController::class, 'active'])->name('product.active');

    Route::get('/inactive/{id}', [ProductController::class, 'inactive'])->name('product.inactive');

    Route::get('/delete/{id}', [ProductController::class, 'delete'])->name('product.delete');
});

Route::prefix('slider')->group(function () {

    Route::get('/index', [SliderController::class, 'index'])->name('slider');

    Route::post('/store', [SliderController::class, 'store'])->name('slider.store');

    Route::get('/edit/{id}', [SliderController::class, 'sliderEdit'])->name('slider.edit');

    Route::post('/update', [SliderController::class, 'sliderUpdate'])->name('slider.update');

    Route::get('/delete/{id}', [SliderController::class, 'sliderDelete'])->name('slider.delete');

    Route::get('/active/{id}', [SliderController::class, 'slideractive'])->name('slider.active');

    Route::get('/inactive/{id}', [SliderController::class, 'sliderInactive'])->name('slider.inactive');
});



// multi language all route

Route::get('/language/english', [LanguageController::class, 'english'])->name('english.language');

Route::get('/language/hindi', [LanguageController::class, 'hindi'])->name('hindi.language');
// product detail page
Route::get('product/detail/{id}/{slug}', [IndexController::class, 'productDetail'])->name('product.detail');
// product tag
Route::get('product/tag/{tag}', [IndexController::class, 'productTag']);
//Subcategory-wise data
Route::get('subcategory/product/{subcat_id}/{slug}', [IndexController::class, 'subCatWiseProduct']);
//Subsubcategory-wise data
Route::get('subsubcategory/product/{subsubcat_id}/{slug}', [IndexController::class, 'subSububCatWiseProduct']);
//Product view model
Route::get('product/view/model/{id}', [IndexController::class, 'productViewAjax']);
//Add to cart
Route::post('/cart/data/store/{id}', [CartController::class, 'addToCart']);
//Add to mini cart
Route::get('/product/mini/cart/', [CartController::class, 'addMiniCart']);
//Remove to mini cart
Route::get('/minicart/product-remove/{rowId}', [CartController::class, 'removeMiniCart']);

Route::post('/add-to-wishlist/{product_id}', [CartController::class, 'addToWishList'])->name('addToWishList');

Route::group(
    //Add to wishlist
    ['prefix' => 'user', 'middleware' => ['user', 'auth'], 'namespace' => 'User'],
    function () {

        Route::get('/wishlist', [WishlistController::class, 'wishlist'])->name('wishlist');

        Route::get('/get-wishlist/product/', [WishlistController::class, 'getWishlist']);

        Route::get('/wishlist-remove/{id}', [WishlistController::class, 'removeWishlist']);

        Route::post('/stripe-order/', [StripeController::class, 'stripeOrder'])->name('stripe.order');

        Route::get('/my/order/', [AllUserController::class, 'myOrders'])->name('my.order');

        Route::get('/order_details/{order_id}', [AllUserController::class, 'detailOrders']);

        Route::post('/cash-order/', [CashController::class, 'cashOrder'])->name('cash.order');

        Route::get('/invoice_downlaod/{order_id}', [AllUserController::class, 'invoice']);

        Route::post('/return.order/{order_id}', [AllUserController::class, 'returnOrder'])->name('return.order');

        Route::get('/return/order/list/', [AllUserController::class, 'returnOrderList'])->name('return.order.list');

        Route::get('/cancel/order/list/', [AllUserController::class, 'cancelOrderList'])->name('cancel.order.list');
    }
);
//myCart
Route::get('/myCart', [CartPageController::class, 'myCart'])->name('myCart');

Route::get('/user/get-cart/product/', [CartPageController::class, 'getCart']);

Route::get('/user/cart-remove/{rowId}', [CartPageController::class, 'removeCart']);

Route::get('/cart-increment/{rowId}', [CartPageController::class, 'cartIncrement']);

Route::get('/cart-decrement/{rowId}', [CartPageController::class, 'cartDecrement']);

//Coupon Route

Route::prefix('coupons')->group(function () {

    Route::get('/view', [CouponController::class, 'couponView'])->name('manage-coupon');

    Route::post('/store', [CouponController::class, 'couponStore'])->name('coupon.store');

    Route::get('/edit/{id}', [CouponController::class, 'couponEdit'])->name('coupon.edit');

    Route::post('/update/{id}', [CouponController::class, 'couponUpdate'])->name('coupon.update');

    Route::get('/delete/{id}', [CouponController::class, 'couponDelete'])->name('coupon.delete');
});

//Shipping Area Division
Route::prefix('shipping')->group(function () {

    Route::get('/view', [ShippingAreaController::class, 'divisionView'])->name('manage-division');

    Route::post('/store', [ShippingAreaController::class, 'divisionStore'])->name('division.store');

    Route::get('/edit/{id}', [ShippingAreaController::class, 'divisionEdit'])->name('division.edit');

    Route::post('/update/{id}', [ShippingAreaController::class, 'divisionUpdate'])->name('division.update');

    Route::get('/delete/{id}', [ShippingAreaController::class, 'divisionDelete'])->name('division.delete');

    //Shipping Area District

    Route::get('/district/view', [ShippingAreaController::class, 'districtView'])->name('manage-district');

    Route::get('/district/ajax/{division_id}', [ShippingAreaController::class, 'districtGetAjax']);

    Route::post('/district/store', [ShippingAreaController::class, 'districtStore'])->name('district.store');

    Route::get('/district/edit/{id}', [ShippingAreaController::class, 'districtEdit'])->name('district.edit');

    Route::post('/district/update/{id}', [ShippingAreaController::class, 'districtUpdate'])->name('district.update');

    Route::get('/district/delete/{id}', [ShippingAreaController::class, 'districtDelete'])->name('district.delete');

    //Shipping Area District

    Route::get('/state/view', [ShippingAreaController::class, 'stateView'])->name('manage-state');

    Route::post('/state/store', [ShippingAreaController::class, 'stateStore'])->name('state.store');

    Route::get('/state/edit/{id}', [ShippingAreaController::class, 'stateEdit'])->name('state.edit');

    Route::post('/state/update/{id}', [ShippingAreaController::class, 'stateUpdate'])->name('state.update');

    Route::get('/state/delete/{id}', [ShippingAreaController::class, 'stateDelete'])->name('state.delete');
});

// frontend coupon
Route::post('/coupon-apply', [CartController::class, 'couponApply']);

Route::get('/coupon-calculation', [CartController::class, 'couponCal']);

Route::get('/coupon-remove', [CartController::class, 'couponRemove']);

// Checkout page
Route::get('/checkout', [CartController::class, 'checkoutCreate'])->name('checkout');

Route::get('/district-get/ajax/{division_id}', [CheckoutController::class, 'districtGetAjax']);

Route::get('/state-get/ajax/{district_id}', [CheckoutController::class, 'stateGetAjax']);

Route::post('/checkout/store', [CheckoutController::class, 'checkoutStore'])->name('checkout.store');

//Orders Routes

Route::prefix('orders')->group(function () {

    Route::get('/pending/view', [OrderController::class, 'pendingOrders'])->name('pending-orders');

    Route::get('/pending/orders/detail/{order_id}', [OrderController::class, 'pendingOrderDetail'])->name('pending.order.detail');

    Route::get('/confirmed/view', [OrderController::class, 'confirmedOrders'])->name('confirmed-orders');

    Route::get('/processing/view', [OrderController::class, 'processingOrders'])->name('processing-orders');

    Route::get('/picked/view', [OrderController::class, 'pickedOrders'])->name('picked-orders');

    Route::get('/shipped/view', [OrderController::class, 'shippedOrders'])->name('shipped-orders');

    Route::get('/delivered/view', [OrderController::class, 'deliveredOrders'])->name('delivered-orders');

    Route::get('/cancel/view', [OrderController::class, 'cancelOrders'])->name('cancel-orders');

    Route::get('/pending/confirm/{order_id}', [OrderController::class, 'pendingToConfirm'])->name('pending-confirm');

    Route::get('/confirm/processing/{order_id}', [OrderController::class, 'confirmToProcessing'])->name('confirm-processing');

    Route::get('/processing/picked/{order_id}', [OrderController::class, 'processingToPicked'])->name('processing-picked');

    Route::get('/picked/shipped/{order_id}', [OrderController::class, 'pickedToShipped'])->name('picked-shipped');

    Route::get('/shipped/delivered/{order_id}', [OrderController::class, 'shippedToDelivered'])->name('shipped-delivered');

    Route::get('/delivered/cancel/{order_id}', [OrderController::class, 'deliveredToCancel'])->name('delivered-cancel');

    Route::get('/invoice.download/{id}', [OrderController::class, 'invoiceDownload'])->name('invoice.download');
});

// Admin Reports Routes
Route::prefix('reports')->group(function () {

    Route::get('/view', [ReportController::class, 'allReports'])->name('all-reports');

    Route::post('/search/by/date', [ReportController::class, 'searchByDate'])->name('search-by-date');

    Route::post('/search/by/month', [ReportController::class, 'searchByMonth'])->name('search-by-month');

    Route::post('/search/by/year', [ReportController::class, 'searchByYear'])->name('search-by-year');
});

// Admin Reports Routes
Route::prefix('alluser')->group(function () {

    Route::get('/view', [ReportController::class, 'allUserReports'])->name('all-user-reports');
});

Route::prefix('setting')->group(function () {

    Route::get('/site', [SiteSettingController::class, 'siteSetting'])->name('site.setting');

    Route::post('/update', [SiteSettingController::class, 'updateSiteSetting'])->name('update.site.setting');
});

//Return Routes
Route::prefix('return')->group(function () {

    Route::get('/admin/request', [ReturnController::class, 'returnRequest'])->name('return.request');

    Route::get('/approve/{order_id}', [ReturnController::class, 'returnApprove'])->name('return.approve');

    Route::get('/all/request', [ReturnController::class, 'returnAllRequest'])->name('all.request');
});

// ---------Role And Permission------------

// Routes
Route::prefix('admin_user_role')->group(function () {

    Route::get('/admin/request', [AdminUserController::class, 'allAdminRole'])->name('all.admin.user');

    Route::get('/add', [AdminUserController::class, 'addAdminRole'])->name('add.admin');

    Route::post('/store', [AdminUserController::class, 'storeAdminRole'])->name('admin.user');

    Route::get('/edit/{id}', [AdminUserController::class, 'editAdminRole'])->name('edit.admin.user');

    Route::post('/update', [AdminUserController::class, 'updateAdminRole'])->name('update.admin.user');

    Route::get('/delete/{id}', [AdminUserController::class, 'deleteAdminRole'])->name('delete.admin.user');
});
//Search Route
Route::post('/search', [SearchController::class, 'searchProduct'])->name('search.product');

Route::post('search-product', [SearchController::class, 'adSearchProduct']);
